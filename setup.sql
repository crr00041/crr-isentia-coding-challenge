CREATE USER isentia PASSWORD 'isentia';
ALTER USER isentia WITH CREATEDB;
CREATE DATABASE sites WITH OWNER isentia;
\c sites isentia;
DROP TABLE IF EXISTS sites;
CREATE TABLE sites (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    url TEXT NOT NULL,
    time TIMESTAMP NOT NULL,
    content TEXT NOT NULL
)